#parse( "header.include.vm" )
#parse( "macros.include.vm" )
#parse( "commands.definition.vm" )
## 只在thrift_client,thrifty_client时生成
#if(!$codewriter.getPropertyExplodedAsList("template.folder.include").contains("thrift_client")   && 
 	  !$codewriter.getPropertyExplodedAsList("template.folder.include").contains("thrifty_client")  &&
 	  !$codewriter.getPropertyExplodedAsList("custom.template.include").contains("command.adapter.container.java.vm"))
#set($codewriter.saveCurrentFile = false)
#stop
#end
#set ( $javaClassName = 'CommandAdapterContainer' )
$codewriter.setCurrentJavaFilename($extensionPkg, "${javaClassName}.java")

package $extensionPkg;

import java.net.URL;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * 设备命令执行器容器对象<br>
 * 允许应用项目用不同的{@link CommandAdapter}对象分别实现设备命令,并支持命令执行器的动态的更新<br>
 * 内部实现为通过命令执行器控制表(Map)管理每个命令对应的执行器.
 * @author guyadong
 *
 */
public class CommandAdapterContainer extends CommandAdapter{
    /** 命令执行器控制表,每一个设备命令对应一个执行器对象,默认为空 */
    private final Map<Cmd, CommandAdapter> adapters= Collections.synchronizedMap(new EnumMap<Cmd, CommandAdapter>(Cmd.class));
    
    public CommandAdapterContainer() {
        this(null);
    }
    /**
     * 用指定的一组命令执行器初始化命令执行器控制表,为{@code null}则忽略
     */
    public CommandAdapterContainer(Map<Cmd, CommandAdapter> adapters) {
        if(null != adapters){
            for(Entry<Cmd, CommandAdapter> entry:adapters.entrySet()){
            	register(entry.getKey(),entry.getValue());
            }
        }
    }
    /**
     * 返回{@code cmd}注册的命令执行器对象,如果没有返回{@code null}
     * @param cmd
     * @return
     */
    public CommandAdapter adapterOf(Cmd cmd) {
        return adapters.get(cmd);
    }
    /**
     * 注册指定命令({@code cmd})的命令执行器
     * @param cmd 设备命令类型,不可为{@code null}
     * @param adapter  命令执行器,不可为{@code null},也不可为容器对象{@link CommandAdapterContainer}
     * @return
     * @see EnumMap#put(Enum, Object)
     */
    public CommandAdapterContainer register(Cmd cmd, CommandAdapter adapter) {
        checkArgument(null != cmd && null != adapter,"key or adapter is null");
        checkArgument(!(adapter instanceof CommandAdapterContainer),"adapter for %s must not be container",cmd);
        adapters.put(cmd, adapter);
        return this;
    }
    /**
     * 注销{@code cmd}指定的命令执行器
     * @param cmd
     * @return 返回被删除的命令执行器
     * @see EnumMap#remove(Object)
     */
    public CommandAdapterContainer unregister(Cmd cmd) {
        adapters.remove(cmd);
        return this;
    }
    
    /**
     * 删除所有命令执行器
     * @see EnumMap#clear()
     */
    public CommandAdapterContainer clear() {
        adapters.clear();
        return this;
    }
#foreach($entry in $commands.entrySet())
#set($key = $entry.key)
#set($value = $entry.value)
#set($params = $value['params'].entrySet())
#set($adapterVar = "${key}Adapter")
    /** 
     * 调用注册的 {@code ${key}} 命令执行器<br>
     * 如果没有为 {@code ${key}} 注册命令执行器,则调用父类方法抛出{@link UnsupportCmdException}异常
     */
    @Override
    public $value['return'] ${key}(#join($params '$e.value[0] $e.key' ','))throws DeviceCmdException{
#define($paramList)#join($params '$e.key' ',')#end
#if($value['return'] == 'void')#set($return = '')#else#set($return = 'return ')#end
        CommandAdapter adapter = this.adapters.get(Cmd.${key});
        if(null != adapter){
            $!{return}adapter.${key}($paramList);
        }else{
            $!{return}super.${key}($paramList);
        }
    }
#end
}